# ymf-springcloud

#### 介绍
my spring cloud demo
* [整体预览](https://gitee.com/brahman-dream/ymf-springcloud/wikis/%E6%95%B4%E4%BD%93%E9%A2%84%E8%A7%88)

### 开发环境
JDK1.8、Maven、IntelliJ IDEA 、Spring Boot2.1.5、Spring Cloud Greenwich.SR1、
MySQL5.1.46、Mybatis2.1.5、RabbitMQ3.6.5

#### 软件架构
 **Spring Cloud 体系技术综合应用说明** 
![输入图片说明](README_img/1561004983897.png)

 **Eureka注册中心说明** 

![输入图片说明](README_img/1560439174201.png)

 **负载均衡Ribbon** 
- 负载均衡是一个算法，可以通过该算法实现从地址列表中获取一个地址进行服务调用。在Spring Cloud中提供了负载均衡器：Ribbon
- Ribbon提供了轮询、随机两种负载均衡算法（默认是轮询）可以实现从地址列表中使用负载均衡算法获取地址进行服务调用。

 **熔断器Hystrix** 
- Hystrix是一个延迟和容错库，用于隔离访问远程服务，防止出现级联失败。

 **线程隔离&服务降级** 
- Hystrix解决雪崩效应：
    * 线程隔离：用户请求不直接访问服务，而是使用线程池中空闲的线程访问服务，加速失败判断时间。
    * 服务降级：及时返回服务调用失败的结果，让线程不因为等待服务而阻塞。
    *降级逻辑
    ```
    @RestController
    @RequestMapping("/consumer")
    @Slf4j
    @DefaultProperties(defaultFallback = "defaultFallback")
    public class ConsumerController {
    
        @Autowired
        private RestTemplate restTemplate;
    
        @Autowired
        private DiscoveryClient discoveryClient;
    
        @GetMapping("/{id}")
        //@HystrixCommand(fallbackMethod = "queryByIdFallback")
        @HystrixCommand
        public String queryById(@PathVariable Long id){
            /*String url = "http://localhost:9091/user/"+id;
    
            //获取eureka中注册的user-service的实例
            List<ServiceInstance> serviceInstances = discoveryClient.getInstances("user-service");
            ServiceInstance serviceInstance = serviceInstances.get(0);
    
            url = "http://" + serviceInstance.getHost() + ":" + serviceInstance.getPort() + "/user/" + id;*/
            String url = "http://user-service/user/" + id;
            return restTemplate.getForObject(url, String.class);
        }
    
        public String queryByIdFallback(Long id){
            log.error("查询用户信息失败。id：{}", id);
            return "对不起，网络太拥挤了！";
        }
    
        public String defaultFallback(){
            return "默认提示：对不起，网络太拥挤了！";
        }
    }
    ```

 **熔断器工作原理** 
![输入图片说明](README_img/1560682028169.png)

 **Feign** 
- 自动根据参数拼接http请求地址。
-  **Feign负载均衡及熔断** 
    * 负载均衡
    * 服务熔断
    * 请求压缩
    * 日志级别
    都可以通过配置项在Feign中开启使用。

 **Spring Cloud Gateway网关** 
- Spring Cloud Gateway的核心就是一系列的过滤器，可以将客户端的请求转发到不同的微服务。主要作用：过滤和路由。
-  **面向服务的路由**
    - 如果将路由服务地址写死明显是不合理的；在Spring Cloud Gateway中可以通过配置动态路由解决。
    - 面向服务的路由；只需要在配置文件中指定路由路径类似： `lb://servicename`
        -  _lb 之后编写的服务名必须要在eureka中注册才能使用_ 

 **路由前缀处理** 
- 客户端的请求地址与微服务的服务地址如果不一致的时候，可以通过配置路径过滤器实现路径前缀的添加和去除。

 **Gateway默认过滤器** 
* 用法：在配置文件中指定要使用的过滤器名称；
* 类型：局部、全局；
* 使用场景：请求鉴权、异常处理、记录调用时长等。

 **自定义局部过滤器** 
```
package com.ymf.gateway.filter;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class MyParamGatewayFilterFactory extends AbstractGatewayFilterFactory<MyParamGatewayFilterFactory.Config> {

    static final String PARAM_NAME = "param";
    public MyParamGatewayFilterFactory() {
        super(Config.class);
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Arrays.asList(PARAM_NAME);
    }

    @Override
    public GatewayFilter apply(Config config) {
        return ((exchange, chain) -> {
            // http://localhost:10010/api/user/8?name=ymf   config.param ==> name
            //获取请求参数中param对应的参数名 的参数值
            ServerHttpRequest request = exchange.getRequest();
            if (request.getQueryParams().containsKey(config.param)){
                request.getQueryParams().get(config.param).
                        forEach(value -> System.out.printf("--------------------局部过滤器--------%s = %s---------\n",config.param,value));
            }
            return chain.filter(exchange);
        });
    }

    public static class Config{
        //对应在配置过滤器的时候指定的参数名
        private String param;

        public String getParam() {
            return param;
        }

        public void setParam(String param) {
            this.param = param;
        }
    }
}

```

 **自定义全局过滤器** 
```
@Component
public class MyGlobalFilter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        System.out.println("---------------全局过滤器MyGlobalFilter---------------");
        String token = exchange.getRequest().getQueryParams().getFirst("token");
        if (StringUtils.isBlank(token)){
            //设置响应状态码为未授权
            exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
            return exchange.getResponse().setComplete();
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        //值越小越先执行
        return 1;
    }
}
```

 **Gateway网关的负载均衡和熔断参数配置** 
- 网关服务配置文件：
- Gateway网关一般直接给终端请求使用；Feign一般用在微服务之间调用。
```
server:
  port: 10010
spring:
  application:
    name: api-gateway
  cloud:
    gateway:
      routes:
#        路由id，可以任意
        - id: user-service-route
#          代理的服务地址
#          uri: http://127.0.0.1:9091
          uri: lb://user-service
#          路由断言：可以匹配映射路径
          predicates:
#            - Path=/user/**
#            - Path=/**
            - Path=/api/user/**
          filters:
#           添加请求路径前缀
#            - PrefixPath=/user
#            1表示过滤一个路径，2表示两个路径，以此类推
            - StripPrefix=1
            - MyParam=name
#     默认过滤器，对所有路由都生效
      default-filters:
        - AddResponseHeader=X-Response-Foo, Bar
        - AddResponseHeader=abc-myname,ymf
#      跨域配置
      globalcors:
        corsConfigurations:
          '[/**]':
            #allowedOrigins: * # 这种写法或者下面的都可以，*表示全部
            allowedOrigins:
              - "http://docs.spring.io"
            allowedMethods:
              - GET
eureka:
  client:
    service-url:
      defaultZone: http://127.0.0.1:10086/eureka
  instance:
    prefer-ip-address: true
hystrix:
  command:
    default:
      execution:
        isolation:
          thread:
            timeoutInMilliseconds: 6000
ribbon:
  ConnectTimeout: 1000
  ReadTimeout: 2000
  MaxAutoRetries: 0
  MaxAutoRetriesNextServer: 0
```

 **Spring Cloud Config分布式配置中心** 
- 可以通过修改在git仓库中的配置文件实现其它所有微服务的配置文件的修改。
    - 配置中心的配置文件
    ```
    server:
      port: 12000
    spring:
      application:
        name: config-server
      cloud:
        config:
          server:
            git:
              uri: https://gitee.com/goheima/heima-config.git
    eureka:
      client:
        service-url:
          defaultZone: http://127.0.0.1:10086/eureka
    ```
- 获取配置中心配置
    - 配置文件bootstrap.yml
```
spring:
  cloud:
    config:
      # 要与仓库中的配置文件的application保持一致
      name: user
      # 要与仓库中的配置文件的profile保持一致
      profile: dev
      # 要与仓库中的配置文件所属的版本（分支）一样
      label: master
      discovery:
        # 使用配置中心
        enabled: true
        # 配置中心服务名
        service-id: config-server

eureka:
  client:
    service-url:
      defaultZone: http://127.0.0.1:10086/eureka
```


 **Spring Cloud Bus** (需用到RabbitMQ)
- 将git仓库的配置文件更新，在不重启系统的情况下实现及时同步到各个微服务

#### 参与贡献
    - 梵梦
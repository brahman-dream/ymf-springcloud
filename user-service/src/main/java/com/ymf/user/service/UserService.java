package com.ymf.user.service;

import com.ymf.user.mapper.UserMapper;
import com.ymf.user.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;


    /**
     * 根据主键查询用户
     * @param id
     * @return
     */
    public User querById(Long id){
        return userMapper.selectByPrimaryKey(id);
    }
}
